import asyncio
import os

from src.interface import WeatherAsyncUserInterface

from dotenv import load_dotenv

load_dotenv()
API_KEY = os.environ.get("API_TOKEN")


async def main():
    obj = WeatherAsyncUserInterface(API_KEY)

    forecast = await obj.get_forecast(2, "London")
    print(forecast)

    weather_now = await obj.get_weather_now("London")
    print(weather_now)


if __name__ == '__main__':
    asyncio.run(main())
