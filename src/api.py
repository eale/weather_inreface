import posixpath
from typing import Optional
from urllib.parse import urljoin

import aiohttp


class ApiException(Exception):
    def __init__(self, status: int, errors: dict):
        message = f"status: {status}"
        super().__init__(message)
        self.errors = errors


class ApiMethodsAsync:
    async def async_post(self, url: str, headers: dict, data: dict) -> Optional[dict]:
        """
        Custom async post method. Return result if status 200 else raise APIException
        """
        async with aiohttp.ClientSession() as session:
            async with session.post(url, json=data, headers=headers) as resp:
                status = resp.status
                if status == 200:
                    result = await resp.json()
                    return result
                else:
                    error = await resp.json()
                    raise ApiException(status, error)


class WeatherApiAsync(ApiMethodsAsync):
    """
    Async api for https://m3o.com/weather/api
    """
    def __init__(self, api_token: str):
        self.api_token = api_token
        self.headers = {"Content-Type": "application/json",
                        "Authorization": f"Bearer {self.api_token}"}

        self.base_url = "https://api.m3o.com/v1/weather/"

    async def get_forecast(self, days: int, location: str) -> dict:
        data = {"days": days, "location": location}

        url_tabs = ["Forecast"]
        path = posixpath.join(*url_tabs)
        url = urljoin(self.base_url, path)

        result = await self.async_post(url, self.headers, data)
        return result

    async def get_weather_now(self, location):
        data = {"location": location}

        url_tabs = ["Now"]
        path = posixpath.join(*url_tabs)
        url = urljoin(self.base_url, path)

        result = await self.async_post(url, self.headers, data)
        return result
