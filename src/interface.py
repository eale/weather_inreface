from api import WeatherApiAsync
from dto import WeatherNowDTO, ForecastDTO
import functions


class WeatherAsyncUserInterface:
    """
    Async user interface for https://m3o.com/weather/api. Return DTO objects
    """
    def __init__(self, api_token: str):
        self.weather_api = WeatherApiAsync(api_token)

    async def get_forecast(self, days: int, location: str) -> ForecastDTO:
        dto_model = ForecastDTO

        json_result = await self.weather_api.get_forecast(days, location)
        custom_fields_result_functions = {"local_time": functions.get_datetime_by_str,
                                          "date": functions.get_date_by_str,
                                          "sunrise": functions.get_time_by_str,
                                          "sunset": functions.get_time_by_str}

        forecasts = json_result.pop("forecast")

        dict_for_dto = functions.get_dict_with_custom_fields(json_result, custom_fields_result_functions)

        forecast_list = []
        for forecast in forecasts:
            forecast_dict_for_dto = functions.get_dict_with_custom_fields(forecast, custom_fields_result_functions)
            forecast_list.append(forecast_dict_for_dto)

        dict_for_dto["forecast"] = forecast_list
        result = dto_model(**dict_for_dto)

        return result

    async def get_weather_now(self, location: str) -> WeatherNowDTO:
        dto_model = WeatherNowDTO

        json_result = await self.weather_api.get_weather_now(location)
        custom_fields_result_functions = {"local_time": functions.get_datetime_by_str}

        dict_for_dto = functions.get_dict_with_custom_fields(json_result, custom_fields_result_functions)

        result = dto_model(**dict_for_dto)

        return result
