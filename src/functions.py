from datetime import datetime, time, date


def get_time_by_str(str_time: str) -> time:
    str_format = "%I:%M %p"
    result = datetime.strptime(str_time, str_format).time()

    return result


def get_datetime_by_str(datetime_str: str) -> datetime:
    str_format = "%Y-%m-%d %H:%M"
    result = datetime.strptime(datetime_str, str_format)

    return result


def get_date_by_str(date_str: str) -> date:
    str_format = "%Y-%m-%d"
    result = datetime.strptime(date_str, str_format).date()

    return result


def get_dict_with_custom_fields(dict_in: dict, custom_fields_result_functions: dict) -> dict:
    result_dict = {}

    for key, value in dict_in.items():
        if key in custom_fields_result_functions.keys():
            value = custom_fields_result_functions[key](value)

        result_dict[key] = value

    return result_dict
