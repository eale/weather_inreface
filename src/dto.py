from datetime import date, datetime, time
from typing import List

import pydantic


class ForecastObjDTO(pydantic.BaseModel):
    date: date
    max_temp_c: float
    max_temp_f: float
    min_temp_c: float
    min_temp_f: float
    avg_temp_c: float
    avg_temp_f: float
    will_it_rain: bool
    chance_of_rain: int
    condition: str
    icon_url: str
    sunrise: time
    sunset: time
    max_wind_mph: float
    max_wind_kph: float

    class Config:
        allow_mutation = False


class ForecastDTO(pydantic.BaseModel):
    location: str
    region: str
    country: str
    latitude: float
    longitude: float
    timezone: str
    local_time: datetime
    forecast: List[ForecastObjDTO]

    class Config:
        allow_mutation = False


class WeatherNowDTO(pydantic.BaseModel):
    location: str
    region: str
    country: str
    latitude: float
    longitude: float
    timezone: str
    local_time: datetime
    temp_c: int
    temp_f: float
    feels_like_c: int
    feels_like_f: float
    humidity: int
    cloud: int
    daytime: bool
    condition: str
    icon_url: str
    wind_mph: float
    wind_kph: float
    wind_direction: str
    wind_degree: int

    class Config:
        allow_mutation = False
